package edu.upenn.cis.cis455;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;

import edu.upenn.cis.cis455.crawler.DOMParserBolt;
import edu.upenn.cis.cis455.storage.Storage;
import junit.framework.TestCase;
import edu.upenn.cis.cis455.xpathengine.*;
import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.LocalCluster;
import edu.upenn.cis.stormlite.PrintBolt;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.WordCounter;
import edu.upenn.cis.stormlite.WordSpout;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
public class TestDomParser extends TestCase {
	
	XPathEngineImplementation engine = (XPathEngineImplementation) XPathEngineFactory.getXPathEngine();
	
	public List<OccurrenceEvent> traverse(List<Element> elements) {
		List<OccurrenceEvent> returnVals = new ArrayList<OccurrenceEvent>();
		if (elements.size()==0) {
			return returnVals;
		}
		for (Element e: elements) {
			String nodeName = e.nodeName();
			if (!e.baseUri().contains("xml")) {
				//url is html doc
				nodeName= e.nodeName().toLowerCase();
			}
			OccurrenceEvent oe = new OccurrenceEvent(OccurrenceEvent.Type.Open, nodeName, e.baseUri());
			returnVals.add(oe);
			oe = new OccurrenceEvent(OccurrenceEvent.Type.Text, e.ownText().toLowerCase(), e.baseUri());
			returnVals.add(oe);
			List<OccurrenceEvent> temp = traverse(e.children());
			for (OccurrenceEvent ee: temp) {
				returnVals.add(ee);
			}
			oe = new OccurrenceEvent(OccurrenceEvent.Type.Close, nodeName, e.baseUri());
			returnVals.add(oe);
		}
		return returnVals;
	}
	
	protected void setUp() {
		String dbPath = "/vagrant/555-hw2/storage-test-storage/";
		
		if (!Files.exists(Paths.get(dbPath))) {
            try {
                Files.createDirectory(Paths.get(dbPath));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
		
		
//		db = new Storage(dbPath);
		
		}
	
	public static String difference(String str1, String str2) {
	      if (str1 == null) {
	          return str2;
	      }
	      if (str2 == null) {
	          return str1;
	      }
	      int at = indexOfDifference(str1, str2);
	      if (at == -1) {
	          return "";
	      }
	      return str2.substring(at);
	  }

	  public static int indexOfDifference(String str1, String str2) {
	      if (str1 == str2) {
	          return -1;
	      }
	      if (str1 == null || str2 == null) {
	          return 0;
	      }
	      int i;
	      for (i = 0; i < str1.length() && i < str2.length(); ++i) {
	          if (str1.charAt(i) != str2.charAt(i)) {
	              break;
	          }
	      }
	      if (i < str2.length() || i < str1.length()) {
	          return i;
	      }
	      return -1;
	  }
	
	public void testDomParser1() {
		//test parsing html doc
        String urlContent = "<html><head><title>a</title></head><body><h1>a</h1><p>a</p></body></html>";
        Document doc  = Jsoup.parse(urlContent);
    	List<OccurrenceEvent> oes = traverse(doc.children());
    	String res = "";
    	for (OccurrenceEvent o: oes) {
    		res += o.toString();
    	}
    	System.out.println(res);
    	assertTrue(res.equals(urlContent));
	}
	
	public void testDomParser2() {
		//test parsing xml doc
        String urlContent = "<rss>"
        		+ "<channel>"
        		+ "<title>CNN.com - World</title>"
        		+ "<link>http://www.cnn.com/rssclick/WORLD/?section=cnn_world</link>"
        		+ "<description>CNN.com delivers up-to-the-minute news and information on the latest top stories, weather, entertainment, politics and more.</description>"
        		+ "<language>en-us</language>"
        		+ "<copyright>© 2006 Cable News Network LP, LLLP.</copyright>"
        		+ "<pubDate>Fri, 03 Feb 2006 10:01:49 EST</pubDate>"
        		+ "<ttl>10</ttl>"
        		+ "<image>"
        		+ "<title>CNN.com - World</title>"
        		+ "<link>http://www.cnn.com/rssclick/WORLD/?section=cnn_world</link>"
        		+ "<url>http://i.cnn.net/cnn/.element/img/1.0/logo/cnn.logo.rss.gif</url>"
        		+ "<width>144</width>"
        		+ "<height>33</height>"
        		+ "<description>CNN.com delivers up-to-the-minute news and information on the latest top stories, weather, entertainment, politics and more.</description>"
        		+ "</image>"
        		+ "<item>"
        		+ "<title>IAEA set to vote on reporting Iran</title>"
        		+ "<link>http://www.cnn.com/rssclick/2006/WORLD/meast/02/03/iran.wrap/index.html?section=cnn_world</link>"
        		+ "<description>The U.N.'s nuclear watchdog appeared set to report Iran to the U.N. Security Council over the Islamic state's atomic program as backroom negotiations continued Friday.</description>"
        		+ "<pubDate>Fri, 03 Feb 2006 08:03:47 EST</pubDate>"
        		+ "</item>"
        		+ "<item>"
        		+ "<title>Ship with 1,400 lost in Red Sea</title>"
        		+ "<link>http://www.cnn.com/rssclick/2006/WORLD/meast/02/03/egypt.ship/index.html?section=cnn_world</link>"
        		+ "<description>An Egyptian passenger ship said to be carrying more than 1,400 people has disappeared in the Red Sea off the Saudi coast, Egyptian maritime officials say.</description>"
        		+ "<pubDate>Fri, 03 Feb 2006 09:53:43 EST</pubDate>"
        		+ "</item>"
        		+ "<item>"
        		+ "<title>Muslim cartoon anger spreads</title>"
        		+ "<link>http://www.cnn.com/rssclick/2006/WORLD/europe/02/03/cartoon.wrap.reut/index.html?section=cnn_world</link>"
        		+ "<description>Read full story for latest details.</description>"
        		+ "<pubDate>Fri, 03 Feb 2006 09:47:00 EST</pubDate>"
        		+ "</item>"
        		+ "<item>"
        		+ "<title>Baghdad car bombs kill 16</title>"
        		+ "<link>http://www.cnn.com/rssclick/2006/WORLD/meast/02/02/iraq.main/index.html?section=cnn_world</link>"
        		+ "<description>Two car bombs exploded minutes apart in southern Baghdad, killing at least 16 people and wounding more than 90 others, Iraqi police said. The cars were parked near a gas station and at a market when they exploded Thursday evening.</description>"
        		+ "<pubDate>Fri, 03 Feb 2006 04:53:48 EST</pubDate>"
        		+ "</item>"
        		+ "<item>"
        		+ "<title>U.N. adviser: West killing Africa with gun sales</title>"
        		+ "<link>http://www.cnn.com/rssclick/2006/WORLD/africa/02/02/africa.guns.reut/index.html?section=cnn_world</link>"
        		+ "<description>Read full story for latest details.</description>"
        		+ "<pubDate>Thu, 02 Feb 2006 12:50:55 EST</pubDate>"
        		+ "</item>"
        		+ "<item>"
        		+ "<title>U.S. Congress extends Patriot Act</title>"
        		+ "<link>http://www.cnn.com/rssclick/2006/POLITICS/02/02/patriot.act.ap/index.html?section=cnn_world</link>"
        		+ "<description>Read full story for latest details.</description>"
        		+ "<pubDate>Thu, 02 Feb 2006 23:07:04 EST</pubDate>"
        		+ "</item>"
        		+ "</channel>"
        		+ "</rss>";
        Document doc=null;
		try {
			doc = Jsoup.connect("https://crawltest.cis.upenn.edu/cnn/cnn_world.rss.xml").get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	List<OccurrenceEvent> oes = traverse(doc.children());
    	String res = "";
    	for (OccurrenceEvent o: oes) {
    		res += o.toString();
    	}
    	assertTrue(res.toLowerCase().equals(urlContent.toLowerCase()));
	}
}