package edu.upenn.cis.cis455;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.jsoup.Jsoup;

import edu.upenn.cis.cis455.storage.Storage;
import junit.framework.TestCase;
import edu.upenn.cis.cis455.xpathengine.*;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
public class TestXPath extends TestCase {
	
	XPathEngineImplementation engine = (XPathEngineImplementation) XPathEngineFactory.getXPathEngine();
	
	protected void setUp() {
		String dbPath = "/vagrant/555-hw2/storage-test-storage/";
		
		if (!Files.exists(Paths.get(dbPath))) {
            try {
                Files.createDirectory(Paths.get(dbPath));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
		
		
//		db = new Storage(dbPath);
		
	}
	
	public void testIsValid() {
		assertTrue(engine.isValid("/foo/bar/a[text()=\"a\"][contains(text(), \"a\")]"));
		assertTrue(engine.isValid("/foo/bar/aa"));
		assertTrue(engine.isValid("/foo[contains(text(), \"a\")]/bar[text() = \"a\"]/aa "));
		assertFalse(engine.isValid("/foo/bar[contains(text, \"a\")/aa "));
		assertFalse(engine.isValid("a/b"));
		assertFalse(engine.isValid("/foo/bar[text() = \"a]"));	
	}
	
}