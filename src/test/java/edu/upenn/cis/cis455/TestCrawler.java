package edu.upenn.cis.cis455;

import edu.upenn.cis.cis455.crawler.Crawler;
import edu.upenn.cis.cis455.crawler.CrawlerWorker;
import edu.upenn.cis.cis455.crawler.Robot;
import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.*;

import junit.framework.*;


import static org.junit.Assert.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.sleepycat.persist.PrimaryIndex;

import static org.mockito.Mockito.*;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
public class TestCrawler extends TestCase{
	Storage db;
	Crawler c;
	static final Logger logger = LogManager.getLogger(TestCrawler.class);
	protected void setUp() {
		String dbPath = "/vagrant/555-hw2/storage-test/";
		
		if (!Files.exists(Paths.get(dbPath))) {
            try {
                Files.createDirectory(Paths.get(dbPath));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
		db = new Storage(dbPath);
		c = new Crawler("https://crawltest.cis.upenn.edu/", db, 1, 5);
	}
	
	public String getMd5(byte[] input) throws NoSuchAlgorithmException {

        MessageDigest mess = MessageDigest.getInstance("MD5"); 

        byte[] md = mess.digest(input); 

        BigInteger number = new BigInteger(1, md); 

        String hashed = number.toString(16); 
        while (hashed.length() < 32) { 
        	hashed = "0" + hashed; 
        } 
        return hashed; 
	}
	
	
	public void testRobot() {
		String web ="https://crawltest.cis.upenn.edu/";
		URLInfo url = new URLInfo(web);
		Robot robot = new Robot(url.getHostName());
		logger.info("host is {}", robot.host);
		logger.info(robot.disallows);
		assertTrue(robot.disallows.get("*").contains("/marie/"));
		assertTrue(robot.disallows.get("cis455crawler").contains("/marie/private/"));
		assertTrue(robot.disallows.get("cis455crawler").contains("/foo/"));
		assertTrue(robot.delay.get("*").equals(10));
		assertTrue(robot.delay.get("cis455crawler").equals(5));
		assertFalse(robot.isDisallowed("https://crawler.cis.upenn.edu/marie/"));
		assertFalse(robot.isDisallowed("https://crawler.cis.upenn.edu/marie/tdp/"));
		assertTrue(robot.isDisallowed("https://crawler.cis.upenn.edu/marie/private/a.html"));
		robot.markVisitTime();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertTrue(robot.isDelay());
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(robot.lastvisitTime);
		System.out.println(System.currentTimeMillis());
		assertFalse(robot.isDelay());
	}
	
	public void testCrawler1() throws InterruptedException, IOException {
		//crawler delivers correct bytes of content
		CrawlerWorker cw = new CrawlerWorker(c);
		byte[] content = cw.getContent("https://crawltest.cis.upenn.edu/");
		assertEquals(1429, content.length);
		content = cw.getContent("https://crawltest.cis.upenn.edu/cnn/index.html");
		assertEquals(382, content.length);
		System.out.println("in test crawler1 test");
		content = cw.getContent("https://crawltest.cis.upenn.edu/cnn/cnn_world.rss.xml");
		assertEquals(3009, content.length);
		content = cw.getContent("https://crawltest.cis.upenn.edu/international/peoplesdaily_world.xml");
		assertEquals(38867, content.length);
	}
	
	public void testCrawler2() {
		CrawlerWorker cw = new CrawlerWorker(c);
		List<String> urls = cw.findUrls("https://crawltest.cis.upenn.edu/cnn/");
		assertTrue(urls.contains("https://crawltest.cis.upenn.edu/cnn/cnn_law.rss.xml"));
		assertTrue(urls.contains("https://crawltest.cis.upenn.edu/cnn/cnn_allpolitics.rss.xml"));
		assertTrue(urls.contains("https://crawltest.cis.upenn.edu/cnn/cnn_topstories.rss.xml"));
		assertTrue(urls.contains("https://crawltest.cis.upenn.edu/cnn/cnn_us.rss.xml"));
		assertTrue(urls.contains("https://crawltest.cis.upenn.edu/cnn/cnn_world.rss.xml"));
	}
	
	public void testCrawlerDBSync1() throws InterruptedException {
		//test modified and contentSeen
		db = new Storage("/vagrant/555-hw2/sc-test1/");
		c = new Crawler("https://crawltest.cis.upenn.edu/", db, 100, 1000);
		CrawlerWorker cw = new CrawlerWorker(c);
		c.addTask("https://crawltest.cis.upenn.edu/");
		Thread t  = new Thread(cw);
		t.start();
		t.join();
		System.out.println("downloaded url size is "+ cw.urlDownloaded.size());
		System.out.println(cw.urlDownloaded);
//		assertTrue((cw.urlDownloaded.contains("https://crawltest.cis.upenn.edu/misc/weather.xml") && !cw.urlDownloaded.contains("https://crawltest.cis.upenn.edu/misc/moreweather.xml")) ||
//				(!cw.urlDownloaded.contains("https://crawltest.cis.upenn.edu/misc/weather.xml") && cw.urlDownloaded.contains("https://crawltest.cis.upenn.edu/misc/moreweather.xml")));
//		
		db = new Storage("/vagrant/555-hw2/sc-test1/");
		c = new Crawler("https://crawltest.cis.upenn.edu/", db, 100, 1000);
		c.addTask("https://crawltest.cis.upenn.edu/");
		CrawlerWorker cw2 = new CrawlerWorker(c);
		t = new Thread(cw2);
		t.start();
		t.join();
		System.out.println(cw2.urlDownloaded);
	}
	
	public void testCrawlerDBSync2() throws InterruptedException {
		//test synchornized
		db = new Storage("/vagrant/555-hw2/sc-test2/");
		c = new Crawler("https://crawltest.cis.upenn.edu/", db, 10, 1000);
		c.addTask("https://crawltest.cis.upenn.edu/");
		c.addTask("https://crawltest.cis.upenn.edu/");
		CrawlerWorker cw = new CrawlerWorker(c);
		CrawlerWorker cw2 = new CrawlerWorker(c);
		Thread t1 = new Thread(cw);
		Thread t2 = new Thread(cw2);
		t1.start();
		t2.start();
		t1.join();
		t2.join();
		Set<String> s1 = new HashSet<String>(cw.urlDownloaded);
		Set<String> s2 = new HashSet<String>(cw2.urlDownloaded);
		s1.retainAll(s2);
		assertEquals(0, s1.size());
	}
	
	
	
}