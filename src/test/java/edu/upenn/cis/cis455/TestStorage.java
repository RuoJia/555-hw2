package edu.upenn.cis.cis455;

import edu.upenn.cis.cis455.storage.*;

import junit.framework.*;


import static org.junit.Assert.*;

import org.junit.Test;

import com.sleepycat.persist.PrimaryIndex;

import static org.mockito.Mockito.*;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
public class TestStorage extends TestCase{
	Storage db;
	private PrimaryIndex<String, User> userIndex;
	private PrimaryIndex<String, DocFile> contentIndex;
	
	protected void setUp() {
		String dbPath = "/vagrant/555-hw2/storage-test-storage/";
		
		if (!Files.exists(Paths.get(dbPath))) {
            try {
                Files.createDirectory(Paths.get(dbPath));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
		
		
		db = new Storage(dbPath);
	}
	
	public String getMd5(byte[] input) throws NoSuchAlgorithmException {

        MessageDigest mess = MessageDigest.getInstance("MD5"); 

        byte[] md = mess.digest(input); 

        BigInteger number = new BigInteger(1, md); 

        String hashed = number.toString(16); 
        while (hashed.length() < 32) { 
        	hashed = "0" + hashed; 
        } 
        return hashed; 
	}
	
	
	public void testDocFunctionalites() {
		db.addDocument("https://www.test.com", "contentString".getBytes(), "text/plain", 0, 0);
		db.addDocument("http://www.test2.com", "contentString2".getBytes(), "text/html", 0, 0);
		
		assertEquals("contentString2", new String(db.getDocument("http://www.test2.com")));
		assertEquals("contentString", new String(db.getDocument("https://www.test.com")));
		assertEquals(2, db.getCorpusSize());
		assertTrue(db.containsContent("https://www.test.com"));
		try {
			assertTrue(db.contentSeen.contains(getMd5("contentString".getBytes())));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			assertTrue(db.contentSeen.contains(getMd5("contentString2".getBytes())));
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void testUserFunctionalites() {
		db.addUser("testName", "testPassword");
		assertTrue(db.getSessionForUser("testName", "testPassword"));
		db.addUser("testName", "testPassword2");
		assertTrue(db.getSessionForUser("testName", "testPassword2"));
		assertTrue(db.containsUser("testName"));
	}
	
	
	
}