package edu.upenn.cis.cis455;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;

import edu.upenn.cis.cis455.storage.Storage;
import junit.framework.TestCase;
import edu.upenn.cis.cis455.xpathengine.*;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
public class TestPathMatcher extends TestCase {
	Map<String, List<String>>matcher = new HashMap<String, List<String>>();
	
	XPathEngineImplementation engine = (XPathEngineImplementation) XPathEngineFactory.getXPathEngine();
	
	protected void setUp() {
		String dbPath = "/vagrant/555-hw2/storage-test-storage/";
		
		if (!Files.exists(Paths.get(dbPath))) {
            try {
                Files.createDirectory(Paths.get(dbPath));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
		
		
//		db = new Storage(dbPath);
		
	}
	
	public List<OccurrenceEvent> traverse(List<Element> elements) {
		List<OccurrenceEvent> returnVals = new ArrayList<OccurrenceEvent>();
		if (elements.size()==0) {
			return returnVals;
		}
		for (Element e: elements) {
			String nodeName = e.nodeName();
			if (!e.baseUri().contains("xml")) {
				//url is html doc
				nodeName= e.nodeName().toLowerCase();
			}
			OccurrenceEvent oe = new OccurrenceEvent(OccurrenceEvent.Type.Open, nodeName, e.baseUri());
			returnVals.add(oe);
			oe = new OccurrenceEvent(OccurrenceEvent.Type.Text, e.ownText().toLowerCase(), e.baseUri());
			returnVals.add(oe);
			List<OccurrenceEvent> temp = traverse(e.children());
			for (OccurrenceEvent ee: temp) {
				returnVals.add(ee);
			}
			oe = new OccurrenceEvent(OccurrenceEvent.Type.Close, nodeName, e.baseUri());
			returnVals.add(oe);
		}
		return returnVals;
	}
	
	public void testMatching1() {
		//test normal matching
		String[] paths = {"/html/body/h2", "/rss/channel/title[contains(text(), \"sports\")]"};
		String[] urls = {"https://crawltest.cis.upenn.edu/", "https://rss.nytimes.com/services/xml/rss/nyt/Sports.xml", 
				"https://crawltest.cis.upenn.edu/bbc/index.html"};
		Document doc = null;
		XPathEngineImplementation engine = (XPathEngineImplementation) XPathEngineFactory.getXPathEngine();
		engine.setXPaths(paths);
		List<OccurrenceEvent> oes = null;
		for (String url:urls) {
			try {
				doc = Jsoup.connect(url).get();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			System.out.println(doc.outerHtml());
			if (doc!=null) {
				oes =traverse(doc.children());
			}
			
			for (OccurrenceEvent o:oes) {
				boolean[] matching = engine.evaluateEvent(o);
				for (int i=0; i<matching.length;i++) {
					if (matching[i]) {
						System.out.println(matching.toString());
						if (!matcher.containsKey(paths[i])) {
							matcher.put(paths[i], new ArrayList<String>());
							matcher.get(paths[i]).add(url);
						} else {
							matcher.get(paths[i]).add(url);
						}
					}
				}
			}
		//end occurrence event loop
		
		}
//		List<String>path0 = new ArrayList<String>();
//		path0.add(urls[0]);
//		path0.add(urls[2]);
		assertEquals(matcher.get(paths[0]).size(), 2);
		assertEquals(matcher.get(paths[1]).size(), 1);
		
	}
	
	public void testMatching2() {
		// test case sensitivity
		String[] paths = {"/htmL/Body/H2", "/Rss/channel/title[contains(text(), \"sports\")]"};
		String[] urls = {"https://crawltest.cis.upenn.edu/", "https://rss.nytimes.com/services/xml/rss/nyt/Sports.xml", 
				"https://crawltest.cis.upenn.edu/bbc/index.html"};
		Document doc = null;
		XPathEngineImplementation engine = (XPathEngineImplementation) XPathEngineFactory.getXPathEngine();
		engine.setXPaths(paths);
		List<OccurrenceEvent> oes = null;
		for (String url:urls) {
			try {
				doc = Jsoup.connect(url).get();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			System.out.println(doc.outerHtml());
			if (doc!=null) {
				oes =traverse(doc.children());
			}
			
			for (OccurrenceEvent o:oes) {
				boolean[] matching = engine.evaluateEvent(o);
				for (int i=0; i<matching.length;i++) {
					if (matching[i]) {
						System.out.println(matching.toString());
						if (!matcher.containsKey(paths[i])) {
							matcher.put(paths[i], new ArrayList<String>());
							matcher.get(paths[i]).add(url);
						} else {
							matcher.get(paths[i]).add(url);
						}
					}
				}
			}
		//end occurrence event loop
		
		}
//		List<String>path0 = new ArrayList<String>();
//		path0.add(urls[0]);
//		path0.add(urls[2]);
		assertEquals(matcher.get(paths[0]).size(), 2);
		assertEquals(matcher.get(paths[1]), null);
	}
}