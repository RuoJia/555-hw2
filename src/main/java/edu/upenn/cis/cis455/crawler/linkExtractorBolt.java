package edu.upenn.cis.cis455.crawler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;

public class linkExtractorBolt implements IRichBolt {
	String executorId = UUID.randomUUID().toString();
	@Override
	public String getExecutorId() {
		// TODO Auto-generated method stub
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		declarer.declare(getSchema());
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
		
	}
	
    public List<String> findUrls(String currentUrl){
    	List<String> nextUrls = new ArrayList<String>();
    	Document document;
    	try {
    		document = Jsoup.connect(currentUrl).get();
    		Elements es = document.select("a[href]");
    		for (Element e: es) {
    			String absHref = e.attr("abs:href");
    			nextUrls.add(absHref);
    		}
    	} catch (IOException e){
    		return nextUrls;
    	}
    	return nextUrls;
    }

	@Override
	public void execute(Tuple input) {
		// TODO Auto-generated method stub
		String currentUrl = input.getStringByField("url");
		
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setRouter(IStreamRouter router) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Fields getSchema() {
		// TODO Auto-generated method stub
		return new Fields();
	}
	
}