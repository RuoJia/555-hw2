package edu.upenn.cis.cis455.crawler;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.parser.Parser;
import org.w3c.tidy.Tidy;

import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;

public class DOMParserBolt implements IRichBolt {
	static final Logger logger = LogManager.getLogger(fetcherBolt.class);
	String executorId = UUID.randomUUID().toString();
	OutputCollector collector;
	static Crawler crawler;
	
	public DOMParserBolt() {
	}
	
	public static void setCrawler(Crawler c) {
		crawler = c;
	}
	
	@Override
	public String getExecutorId() {
		// TODO Auto-generated method stub
		return executorId;
	}
	

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		declarer.declare(getSchema());
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
		
	}
	
	//DFS
	public void traverse(List<Element> elements) {
		if (elements.size()==0) {
			return;
		}
		for (Element e: elements) {
			String nodeName = e.nodeName();
			if (!e.baseUri().contains("xml")) {
				//url is html doc
				nodeName= e.nodeName().toLowerCase();
			}
			OccurrenceEvent oe = new OccurrenceEvent(OccurrenceEvent.Type.Open, nodeName, e.baseUri());
			collector.emit(new Values<Object>(oe, e.baseUri()));
			oe = new OccurrenceEvent(OccurrenceEvent.Type.Text, e.ownText().toLowerCase(), e.baseUri());
			collector.emit(new Values<Object>(oe, e.baseUri()));
			traverse(e.children());
			oe = new OccurrenceEvent(OccurrenceEvent.Type.Close, nodeName, e.baseUri());
			collector.emit(new Values<Object>(oe, e.baseUri()));
		}
		return;
	}
	

	@Override
	public void execute(Tuple input) {
		// TODO Auto-generated method stub
		String contentType = input.getStringByField("type");
		String url = input.getStringByField("url");
		byte[] content = (byte[]) input.getObjectByField("urlContent");
		ByteArrayInputStream in = new ByteArrayInputStream(content);
		if (contentType.contains("text") || contentType.contains("xml")) {
			Document doc = null;
			try {
				doc = Jsoup.connect(url).get();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (doc!=null) {
				traverse(doc.children());
			}
		} 
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		// TODO Auto-generated method stub
		this.collector = collector;
	}

	@Override
	public void setRouter(IStreamRouter router) {
		// TODO Auto-generated method stub
		this.collector.setRouter(router);
	}

	@Override
	public Fields getSchema() {
		// TODO Auto-generated method stub
		return new Fields("event", "url");
	}
	
}