package edu.upenn.cis.cis455.crawler;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import edu.upenn.cis.cis455.crawler.utils.URLInfo;

public class CrawlerWorker implements Runnable{
	static final Logger logger = LogManager.getLogger(CrawlerWorker.class);
	Crawler c;
	public List<String>urlDownloaded;
	public CrawlerWorker(Crawler c) {
		this.c = c;
		this.urlDownloaded = new ArrayList<String>();
	}
	
    public Object[] sendHead(String currentUrl) throws IOException {
    	logger.info("cralwer sending head request to {}", currentUrl);
    	if (currentUrl.startsWith("https")) {
    		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
    		URL httpsUrl = new URL(currentUrl);
    		HttpsURLConnection cn = (HttpsURLConnection) httpsUrl.openConnection();
    		cn.setRequestMethod("HEAD");
    		cn.addRequestProperty("Host", httpsUrl.getHost());
    		cn.addRequestProperty("User-agent", "cis455crawler");
    		cn.connect();
    		int contentLength = -1;
			long lastModified  = -1;
			String contentType = "text/html";
    		int statusCode =  cn.getResponseCode();
    		if (statusCode >=200 && statusCode <=400) {
    			contentLength = cn.getContentLength();
    			lastModified = cn.getLastModified();
    			contentType = cn.getContentType();
    			return new Object[] {contentLength, contentType, lastModified, true};	
    		}
    	} else if (currentUrl.startsWith("http")) {
    		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
    		URL httpsUrl = new URL(currentUrl);
    		HttpURLConnection cn = (HttpURLConnection) httpsUrl.openConnection();
    		cn.setRequestMethod("HEAD");
    		cn.addRequestProperty("Host", httpsUrl.getHost());
    		cn.addRequestProperty("User-agent", "cis455crawler");
    		cn.connect();
    		int contentLength = -1;
			long lastModified  = -1;
			String contentType = "text/html";
    		int statusCode =  cn.getResponseCode();
    		if (statusCode >=200 && statusCode <=400) {
    			contentLength = cn.getContentLength();
    			lastModified = cn.getLastModified();
    			contentType = cn.getContentType();
    			return new Object[] {contentLength, contentType, lastModified, true};	
    		}
    	}
    	
    	return new Object[]{null, null, null, false};
    }
    
    public InputStream sendGet(String currentUrl) throws IOException {
    	logger.info("crawler sending get request to {}", currentUrl);
    	if (currentUrl.startsWith("https")) {
    		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
    		URL httpsUrl = new URL(currentUrl);
    		HttpsURLConnection cn = (HttpsURLConnection) httpsUrl.openConnection();
    		cn.setRequestMethod("GET");
    		cn.addRequestProperty("Host", httpsUrl.getHost());
    		cn.addRequestProperty("User-agent", "cis455crawler");
    		cn.connect();
    		return cn.getInputStream();
    	} else if (currentUrl.startsWith("http")) {
    		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
    		URL httpsUrl = new URL(currentUrl);
    		HttpURLConnection cn = (HttpURLConnection) httpsUrl.openConnection();
    		cn.setRequestMethod("GET");
    		cn.addRequestProperty("Host", httpsUrl.getHost());
    		cn.addRequestProperty("User-agent", "cis455crawler");
    		cn.connect();
    		return cn.getInputStream();
    	}
    	return null;
    }
    
    
    public boolean correctType(String type) {
    	if (type.startsWith("text/html") || type.startsWith("text/xml") 
    			|| type.startsWith("application/xml") || type.endsWith("+xml")) {
    		return true;
    	}
    	return false;
    }
    
    public boolean validContentSize(int contentSize) {
    	if (contentSize >= c.maxSize) {
    		return false;
    	} 
    	return true;
    }
    
    public List<String> findUrls(String currentUrl){
    	List<String> nextUrls = new ArrayList<String>();
    	Document document;
    	try {
    		document = Jsoup.connect(currentUrl).get();
    		Elements es = document.select("a[href]");
    		for (Element e: es) {
    			String absHref = e.attr("abs:href");
    			nextUrls.add(absHref);
    		}
    	} catch (IOException e){
    		return nextUrls;
    	}
    	return nextUrls;
    }
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		logger.info("thread running");
		while (!c.isDone()) {
			//get url to process
			String currentUrl=null;
			try {
				currentUrl = c.getTask();
			}catch (Exception e) {
				System.out.println(e);
			}
//			logger.info("current url is {}", currentUrl);
			URLInfo urlInfo = new URLInfo(currentUrl);
			
			
			//check if we follow robot.txt
			int contentLength = -1;
			long lastModified  = -1;
			String contentType = "text/html";
			String hostName = urlInfo.getHostName();
			Robot robot= null;
			synchronized(c.robotMap) {
				if (!c.robotMap.containsKey(hostName)) {
					//host name has been crawled, we need to add it to robot to check for delay and disallow
					robot = new Robot(hostName);
					c.robotMap.put(hostName, robot);
				} else {
					robot = c.robotMap.get(hostName);
				}
			}
			//check delay and disallow
//			logger.info("checking disallow");
			if(robot.isDisallowed(currentUrl)) {
				System.out.println(currentUrl + " disallows robot");
				continue;
			}
//			logger.info("checking delay");
			if (robot.isDelay()) {
				try {
					c.addTask(currentUrl);
				} catch (Exception e) {
					System.out.println(e);
				}
				continue;
			}
			//send head request
			boolean valid=false;
			Object[] returnVals=new Object[] {null, null, null};
			try {
				returnVals = sendHead(currentUrl);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			valid = (boolean) returnVals[3];
			if (valid) {
				contentLength = (int) returnVals[0];
				contentType = (String)returnVals[1];
				lastModified = (long) returnVals[2];
			}
			
			if (!valid) {
				System.out.println(currentUrl+" head request error");
				continue;
			}
			if (!correctType(contentType)) {
				System.out.println(currentUrl+" content type error");
				continue;
			}
			if (!validContentSize(contentLength)) {
				System.out.println(currentUrl+" content size error");
				continue;
			}
			c.setWorking(valid);
			// process valid url and content
//			byte[] urlContent = getContent(currentUrl);
			byte[] urlContent=null;
			try {
				urlContent = getContent(currentUrl);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (urlContent!=null) {
				String hashContent= "";
				try {
					hashContent = c.currentdb.getMd5(urlContent);
				} catch (NoSuchAlgorithmException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if ((c.currentdb.containsContent(currentUrl) && !c.currentdb.modified(currentUrl, lastModified))
						) {
					//checks if the crawler crawls the file in the previous run
					logger.info("not modified {}", currentUrl);
				} else if (c.currentdb.contentSeen.contains(hashContent)) {
					//checks if the crawler saw the file in the same run
					//if either is true, then the file should not be downloaded
					logger.info("content has been crawled earlier {}", currentUrl);
				} else {
					logger.info("downloading {}", currentUrl);
					synchronized(c) {
						c.incCount();
					}
					urlDownloaded.add(currentUrl);
					logger.info("added to db, now downloaded {} docs", c.currentPageNum);
					synchronized(robot) {
						robot.markVisitTime();
					}
					synchronized(c.currentdb) {
						
						c.currentdb.addDocument(currentUrl, urlContent, contentType, lastModified,lastModified);
						c.currentdb.sync();
					}	
				}
				// extract other urls in currenturl
				if (contentType.contains("html")) {
					List<String> urls = findUrls(currentUrl);
					logger.info("found {} urls", urls.size());
					for (String url: urls) {
						logger.info("{} has link to url: {}", currentUrl, url);
						if (!c.seenUrl.contains(url)) {
							try {
								c.addTask(url);
								synchronized(c.seenUrl) {
									c.seenUrl.add(url);
								}
							} catch (Exception e) {
								logger.info(e);
								System.out.println(e);
							}
						}else {
							
							logger.info("url {} is seen, not added", url);
						}
					}
				}
				logger.info("urls added");		
			}
			
			
			
			
		}
		
		//worker done
		logger.info("Downloaded urls: {}", urlDownloaded);
		c.notifyThreadExited();
	}

	public byte[] getContent(String url) throws IOException {
		// TODO Auto-generated method stub
		InputStream in=null;
		//send get request, return inputstream
		try {
			in = sendGet(url);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (in!=null) {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			in.transferTo(os);
			return os.toByteArray();
		}		
		return null;
	}
	
}