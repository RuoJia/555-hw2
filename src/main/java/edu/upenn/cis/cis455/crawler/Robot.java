package edu.upenn.cis.cis455.crawler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
public class Robot {
	public String host;
	private String targetWeb;
	public Map<String, ArrayList<String>> disallows;
	public Set<String> agentInfo;
	public Map<String, Integer> delay = new HashMap<String, Integer>();
	public long lastvisitTime=0;
	static final Logger logger = LogManager.getLogger(Robot.class);
	//TODO: default delay value;
	public void getRobotTxt() throws IOException {
		logger.info("forming robot.txt info from {}", this.targetWeb);
		InputStream in = sendGet(this.targetWeb);
		if (in==null) {
			return;
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		String line = "";
		while ((line = reader.readLine())!=null) {
			line = line.toLowerCase();
			if (line.startsWith("user-agent: cis455crawler")) {
				parseInfo(reader, "cis455crawler");
			} else if (line.startsWith("user-agent: *")){
				parseInfo(reader, "*");
			}
		}
		logger.info(disallows.get("cis455crawler").toString());
	}
	
	public void parseInfo(BufferedReader reader, String agent) throws IOException {
		String line = "";
		while ((line=reader.readLine())!=null && line.length()!=0) {
			line = line.toLowerCase();
			if (line.startsWith("user-agent: ")) break;
			if (line.startsWith("disallow: ")) {
				String dis = line.substring(10);
				if (dis.endsWith("/")) {
					disallows.get(agent).add(line.substring(10, line.length()-1));
				}
				disallows.get(agent).add(line.substring(10));
			} else if (line.startsWith("crawl-delay: ")) {
				delay.put(agent, Integer.parseInt(line.substring(13)));
			}
		}
	}
	
	public InputStream sendGet(String currentUrl) throws IOException {
    	if (currentUrl.startsWith("https")) {
    		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
    		URL httpsUrl = new URL(currentUrl);
    		HttpsURLConnection cn = (HttpsURLConnection) httpsUrl.openConnection();
    		cn.setRequestMethod("GET");
    		cn.addRequestProperty("Host", httpsUrl.getHost());
    		cn.addRequestProperty("User-agent", "cis455crawler");
    		cn.connect();
    		logger.info("connection is set, returning connection");
    		return cn.getInputStream();
    	} else if (currentUrl.startsWith("http")) {
    		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
    		URL httpUrl = new URL(currentUrl);
    		HttpURLConnection cn = (HttpURLConnection) httpUrl.openConnection();
    		cn.setRequestMethod("GET");
    		cn.addRequestProperty("Host", httpUrl.getHost());
    		cn.addRequestProperty("User-agent", "cis455crawler");
    		cn.connect();
    		logger.info("connection is set, returning connection");
    		return cn.getInputStream();
    	}
    	return null;
	}
	
	public boolean isDisallowed(String currentUrl){
		ArrayList<String> urls;
		if (!disallows.get("cis455crawler").isEmpty()) {
			urls = disallows.get("cis455crawler");
		} else {
			urls = disallows.get("*");
		}
			for (String url: urls) {
				if (currentUrl.equals(url) || currentUrl.contains(url)) {
					return true;
				}
			}
		return false;
	}
	
	public Robot(String hostname) {
		this.host = hostname;
		this.targetWeb =  "http://" + host + "/robots.txt";
		ArrayList<String> temp =  new ArrayList<String>();
		disallows = new HashMap<String, ArrayList<String>>();
		disallows.put("*", temp);
		temp =  new ArrayList<String>();
		disallows.put("cis455crawler", temp);
		delay.put("default", 0);
		try {
			getRobotTxt();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		logger.info(delay);
		logger.info(disallows);
	}
	
	public boolean isDelay() {
		int agentDelay;
		if (delay.get("cis455crawler")!=null) {
			agentDelay = delay.get("cis455crawler");
		} else {
			if (delay.get("*")!=null) {
				agentDelay = delay.get("*");
			} else {
				agentDelay = delay.get("default");
			}
		}
		Long currentTime = Calendar.getInstance().getTimeInMillis();
		if ((currentTime - this.lastvisitTime)/1000 < agentDelay) {
			return true;
		}
		return false;
	}
	
	public synchronized void markVisitTime() {
		this.lastvisitTime = Calendar.getInstance().getTimeInMillis();
	}
	
}