package edu.upenn.cis.cis455.crawler;

import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.stormlite.*;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.spout.IRichSpout;
import edu.upenn.cis.stormlite.spout.SpoutOutputCollector;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Values;

public class crawlerQueueSpout implements IRichSpout{
	static final Logger logger = LogManager.getLogger(crawlerQueueSpout.class);
	
	String executorId = UUID.randomUUID().toString();
	SpoutOutputCollector collector;
	static Crawler crawler;
	
	public crawlerQueueSpout() {}
	
	public static void setCrawler(Crawler c) {
		crawler = c;
	}
	
	
	@Override
	public String getExecutorId() {
		// TODO Auto-generated method stub
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		declarer.declare(new Fields("url"));
	}

	@Override
	public void open(Map<String, String> config, TopologyContext topo, SpoutOutputCollector collector) {
		// TODO Auto-generated method stub
		this.collector = collector;
	}
	
//	public void setParam(Crawler c) {
//		this.crawler = c;
//	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nextTuple() {
		// TODO Auto-generated method stub
		if (!crawler.frontier.isEmpty()) {
			String url =  crawler.frontier.poll();
//			logger.info("get url from queue {}", url);
			if (url!=null) {
				collector.emit(new Values<Object>(url));
			}
		}
	}

	@Override
	public void setRouter(IStreamRouter router) {
		// TODO Auto-generated method stub
		this.collector.setRouter(router);
	}
	
}