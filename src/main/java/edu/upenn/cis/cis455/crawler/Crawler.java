package edu.upenn.cis.cis455.crawler;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.*;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.Channel;
import edu.upenn.cis.cis455.storage.Storage;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import edu.upenn.cis.cis455.xpathengine.XPathEngineFactory;
import edu.upenn.cis.cis455.xpathengine.XPathEngineImplementation;
import edu.upenn.cis.cis455.xpathengine.XPathInfo;
import edu.upenn.cis.stormlite.Config;
import edu.upenn.cis.stormlite.LocalCluster;
import edu.upenn.cis.stormlite.Topology;
import edu.upenn.cis.stormlite.TopologyBuilder;
import edu.upenn.cis.stormlite.bolt.*;
import edu.upenn.cis.stormlite.spout.*;
import edu.upenn.cis.stormlite.tuple.*;

public class Crawler implements CrawlMaster {
    ///// TODO: you'll need to flesh all of this out. You'll need to build a thread
    // pool of CrawlerWorkers etc.
	static final Logger logger = LogManager.getLogger(Crawler.class);
    static final int NUM_WORKERS = 10;
    private String seedUrl;
    public long maxSize;
    public int maxPage;
    public Storage currentdb;
    //TODO: concurrent hashmap, atomic integer
    public BlockingQueue<String> frontier = new LinkedBlockingDeque<String>();
    Set<String> seenUrl = new HashSet<String>();
    Map<String, Robot> robotMap = new HashMap<String, Robot>();
    public Set<Thread> threadPool = new HashSet<Thread>();
    int currentPageNum = 0;
	public int activeThreadNum = 0;
	public XPathEngineImplementation engine;
	public List<String> crawledUrls =  new ArrayList<String>();

    public Crawler(String startUrl, StorageInterface db, int size, int count) {
        // TODO: initialize
    	currentdb  = (Storage) db;
    	maxSize = size*1024*1024;
    	maxPage = count;
    	seedUrl = startUrl;
    	engine = (XPathEngineImplementation) XPathEngineFactory.getXPathEngine();
    	List<String> XPaths = currentdb.getXPathList();
    	engine.setXPaths(XPaths.toArray(new String[XPaths.size()]));
    	
    }
    
    
	public void addTask(String url) throws InterruptedException {
		synchronized(frontier) {
				frontier.add(url);
//				logger.info("url added to frontier {}", url);
				frontier.notifyAll();
		}
	}
	
	public String getTask() throws InterruptedException {
		synchronized(frontier) {
			while (true) {
				if (this.frontier.isEmpty()) {
					frontier.wait();
				} else {
					String task =  frontier.poll();
//					logger.info("got task {}", task);
					frontier.notifyAll();
					return task;
				}
		}
		}
	}
    
    

    /**
     * Main thread
     */
    public void start() {
    	
    	try {
    		addTask(seedUrl);
    	} catch (InterruptedException e) {
    		System.out.println("thread interrupted");
    	}
    	seenUrl.add(seedUrl);
//    	
//    	for (int i = 0; i < 2; i++) {
//    		CrawlerWorker w = new CrawlerWorker(this);
//    		Thread t  = new Thread(w);
//            this.threadPool.add(t);
//            t.start();
//            activeThreadNum ++;
//        }
//    	logger.info("all worker threads have been started");
    	
    		//poll url from frontier
    	//done bfs
    }

    /**
     * We've indexed another document
     */
    @Override
    public synchronized void incCount() {
    	currentPageNum ++;
    }

    /**
     * Workers can poll this to see if they should exit, ie the crawl is done
     */
    @Override
    public boolean isDone() {
    	//TODO: adding one other condition that the last bolt finished
        if (!frontier.isEmpty() && currentPageNum<maxPage) {
        	return false;
        } else {
        	return true;
        }
    }

    /**
     * Workers should notify when they are processing an URL
     */
    @Override
    public void setWorking(boolean working) {
    }

    /**
     * Workers should call this when they exit, so the master knows when it can shut
     * down
     */
    @Override
    public void notifyThreadExited() {
//    	logger.info("thread exiting");
//    	activeThreadNum--;
//    	logger.info("active thread num: {}", activeThreadNum);
    	
    	//TODO: call by last bolt, remind crawler to exit
    }
    
    public List<String> findUrls(String currentUrl){
    	List<String> nextUrls = new ArrayList<String>();
    	Document document;
    	try {
    		document = Jsoup.connect(currentUrl).get();
    		Elements es = document.select("a[href]");
    		for (Element e: es) {
    			String absHref = e.attr("abs:href");
    			nextUrls.add(absHref);
    		}
    	} catch (IOException e){
    		return nextUrls;
    	}
    	return nextUrls;
    }
    
	public List<OccurrenceEvent> traverse(List<Element> elements) {
		List<OccurrenceEvent> returnVals = new ArrayList<OccurrenceEvent>();
		if (elements.size()==0) {
			return returnVals;
		}
		for (Element e: elements) {
			String nodeName = e.nodeName();
			if (!e.baseUri().contains("xml")) {
				//url is html doc
				nodeName= e.nodeName().toLowerCase();
			}
			OccurrenceEvent oe = new OccurrenceEvent(OccurrenceEvent.Type.Open, nodeName, e.baseUri());
			returnVals.add(oe);
			oe = new OccurrenceEvent(OccurrenceEvent.Type.Text, e.ownText().toLowerCase(), e.baseUri());
			returnVals.add(oe);
			List<OccurrenceEvent> temp = traverse(e.children());
			for (OccurrenceEvent ee: temp) {
				returnVals.add(ee);
			}
			oe = new OccurrenceEvent(OccurrenceEvent.Type.Close, nodeName, e.baseUri());
			returnVals.add(oe);
		}
		return returnVals;
	}

    /**
     * Main program: init database, start crawler, wait for it to notify that it is
     * done, then close.
     * @throws IOException 
     * @throws InterruptedException 
     *
     */
    public static void main(String args[]) throws IOException, InterruptedException {
    	org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
        if (args.length < 3 || args.length > 5) {
            System.out.println("Usage: Crawler {start URL} {database environment path} {max doc size in MB} {number of files to index}");
            System.exit(1);
        }

        System.out.println("Crawler starting");
        String startUrl = args[0];
        URLInfo urlInfo = new URLInfo(startUrl);
        String envPath = args[1];
        Integer size = Integer.valueOf(args[2]);
        Integer count = args.length == 4 ? Integer.valueOf(args[3]) : 100;
        
        if (!Files.exists(Paths.get(args[1]))) {
            try {
                Files.createDirectory(Paths.get(args[1]));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        StorageInterface db = StorageFactory.getDatabaseInstance(envPath);

        Crawler crawler = new Crawler(startUrl, db, size, count);
//        List<String> urls = crawler.findUrls(startUrl);
//        urls.add(startUrl);
//        List<String> temp = new ArrayList<String>(urls);
//        for (String url:temp) {
//        	if (url.endsWith("htm")) {
//        		urls.addAll(crawler.findUrls(url));
//        	}
//        }
//        logger.info(urls);
//		List<String> XPaths =  new ArrayList<String>();
//		for (XPathInfo c: crawler.engine.xp) {
//			XPaths.add(c.rawPath);
//		}
//		logger.info(XPaths);
//		Map<String,Channel> XPathsMap = crawler.currentdb.getXPaths().map();
//		Map<String, String> pathToChannelName = new HashMap<String, String>();
//		for (Channel c: XPathsMap.values()) {
//			logger.info("xpath in db is {}", c.XPath);
//			pathToChannelName.put(c.XPath, c.name);
//		}
//        for (String url:urls) {
//        	URL u = new URL(url);
//        	URLConnection connection = u.openConnection();
//        	connection.connect();
//        	String contentType = connection.getContentType();
//        	logger.info("content type is {}", contentType);
//        	if (contentType.contains("text") || contentType.contains("xml")) {
//        		Document doc=null;
//				try {
//					doc = Jsoup.connect(url).get();
//					logger.info(doc.outerHtml());
//				} catch (IOException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				List<OccurrenceEvent> oes = crawler.traverse(doc.children());
////        		logger.info(oes);
//        		for (OccurrenceEvent o:oes) {
//        			boolean[] matching = crawler.engine.evaluateEvent(o);
////        			logger.info("current state is {}", crawler.engine.currentState.get(url).currentPathInfo);
//        			for (int i=0; i<matching.length;i++) {
//        				if (matching[i]) {
////        					logger.info("matching {} to {}", url, crawler.engine.xpMap.get(XPaths.get(i)));
//        					String name = pathToChannelName.get(crawler.engine.xpMap.get(XPaths.get(i)));
//        					logger.info("matching {} to {}, with name {}", url, crawler.engine.xpMap.get(XPaths.get(i)), name);
////        					crawler.currentdb.addDocToChannel(name, url);
//        				}
//        			}
//        		}
//
//        	}
//        }
//        logger.info("matching urls to HTML2: {}", crawler.currentdb.getChannel("HTML2").matchingUrls);
////        crawler =null;
//        
//        System.out.println("Starting crawl of " + count + " documents, starting at " + startUrl);
        // add starting url to queue
//        crawler = null;
        crawler.start();
//        crawler.addTask(startUrl);
//        crawler.seenUrl.add(startUrl);
//    	try {
//			Thread.sleep(100);
//	    	logger.info("urls crawled {}", crawler.crawledUrls);
//	        // TODO: final shutdown
//	        logger.info(crawler.seenUrl);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
        //initialize topology config
    	Config conf = new Config();
    	crawlerQueueSpout spout = new crawlerQueueSpout();
    	crawlerQueueSpout.setCrawler(crawler);
    	fetcherBolt fetcher = new fetcherBolt();
    	fetcherBolt.setCrawler(crawler);
    	DOMParserBolt parser = new DOMParserBolt();
    	DOMParserBolt.setCrawler(crawler);
    	pathMatcherBolt matcher = new pathMatcherBolt();
    	pathMatcherBolt.setCrawler(crawler);
    	
    	TopologyBuilder builder = new TopologyBuilder();
    	builder.setSpout("QUEUESPOUT", spout, 1);
    	builder.setBolt("FETCHERBOLT", fetcher, 1).shuffleGrouping("QUEUESPOUT");
    	builder.setBolt("PARSERBOLT", parser, 1).shuffleGrouping("FETCHERBOLT");
    	builder.setBolt("MATCHERBOLT", matcher, 1).fieldsGrouping("PARSERBOLT", new Fields("url"));
    	
    	LocalCluster cluster = new LocalCluster();
    	Topology tp = builder.createTopology();
    	ObjectMapper mapper = new ObjectMapper();
    	String str="";
		try {
			str = mapper.writeValueAsString(tp);
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
    	logger.info("topology is {}", str);
    	cluster.submitTopology("testing", conf, tp);
    	while (!crawler.isDone()) {
    		try {
    			Thread.sleep(10);
    		} catch (InterruptedException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		if (crawler.isDone()) {
    			//if queue is empty, wait for 2 sec,
    			try {
    				System.out.println("wait for 10 sec");
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				};
				if (crawler.isDone()) {
					//if queue is still empty, done
					cluster.killTopology("testing");
	        		cluster.shutdown();
	        		break;
				}
        	}
    	}
    	logger.info("urls crawled {}", crawler.crawledUrls);
        // TODO: final shutdown
        logger.info(crawler.seenUrl);
        crawler.currentdb.sync();
        System.out.println("Done crawling! Exiting.");
        System.exit(0);
    }

}
