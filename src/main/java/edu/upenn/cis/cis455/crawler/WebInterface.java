package edu.upenn.cis.cis455.crawler;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sleepycat.persist.PrimaryIndex;

import static spark.Spark.*;
import edu.upenn.cis.cis455.crawler.handlers.LoginFilter;
import edu.upenn.cis.cis455.storage.Channel;
import edu.upenn.cis.cis455.storage.DocFile;
import edu.upenn.cis.cis455.storage.Storage;
import edu.upenn.cis.cis455.storage.StorageFactory;
import edu.upenn.cis.cis455.storage.StorageInterface;
import edu.upenn.cis.cis455.xpathengine.XPathEngineImplementation;
import spark.*;
//import edu.upenn.cis.cis455.m2.implementations.HttpSession;
//import edu.upenn.cis.cis455.m2.interfaces.Session;

//import static edu.upenn.cis.cis455.SparkController.*;
import edu.upenn.cis.cis455.crawler.handlers.LoginHandler;
import edu.upenn.cis.cis455.crawler.handlers.RegistrationHandler;

public class WebInterface {
	static final Logger logger = LogManager.getLogger(WebInterface.class);
    public static void main(String args[]) {
    	org.apache.logging.log4j.core.config.Configurator.setLevel("edu.upenn.cis.cis455", Level.DEBUG);
        if (args.length < 1 || args.length > 2) {
            System.out.println("Syntax: WebInterface {path} {root}");
            System.exit(1);
        }

        if (!Files.exists(Paths.get(args[0]))) {
            try {
                Files.createDirectory(Paths.get(args[0]));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        port(45555);
        Storage database = (Storage) StorageFactory.getDatabaseInstance(args[0]);

        LoginFilter testIfLoggedIn = new LoginFilter(database);

        if (args.length == 2) {
//            staticFiles.externalLocation(args[1]);
            staticFileLocation(args[1]);
        }


        before("/*", "POST", testIfLoggedIn);
//        before("/*", "GET", testIfLoggedIn);
        //get homepage
        get("/index.html", (request, response)->{
        	Session s = request.session(false);
        	if (s==null) {
        		return null;
        	}
        	String user = (String) s.attribute("user");
        	logger.info("current user is {}", user);
        	String res = "<!DOCTYPE html>\n"
        			+ "<html>\n"
        			+ "<head>\n"
        			+ "    <title>Homepage</title>\n"
        			+ "</head>\n"
        			+ "<body>";
        	res += "<p>" + "Welcome " + user + "</p>";
        	//create channel
        	res += "<form method=\"GET\" action=\"/create\">\n"
        			+ "Channel Name: "
        			+ "<input type=\"text\" name=\"name\" />\n"
        			+ "XPath: " 
        			+ "<input type=\"text\" name=\"xpath\" />\n"
        			+"<input type=\"submit\" value=\"Create Channel\"/>\n"
        			+ "</form>";
        	//add channel info
        	PrimaryIndex<String, Channel> channels = database.channelIndex;
        	res += "<p> Current channels available: </p>\n";
        	if (channels.count()!=0) {
        		for (String cname: channels.keys()) {
            		res += "<li>" + cname + "<a href=/show?channel=" + cname+">" + "link" +"</a></li>";
            	}
        	}
        	res += "\n";
        	res += "<form method=\"POST\" action=\"/logout\">\n"
        			+ "<input type=\"submit\" value=\"log out\"/>\n"
        			+ "</form>";
        	res += "</body></html>";
//        	response.body(res);
        	return res;
        });
       
        
        // TODO:  add /register, /logout, /index.html, /, /lookup
        post("/register",  new RegistrationHandler(database));
        post("/login",  new LoginHandler(database));
        post("/logout", (request, response)->{
        	request.session(false).invalidate();
        	response.redirect("/login");
        	return null;
        });
        
        get("/", (request, response)->{
        	Session s = request.session(false);
        	if (s==null) {
        		return null;
        	}
        	String user = (String) s.attribute("user");
        	logger.info("current user is {}", user);
        	String res = "<!DOCTYPE html>\n"
        			+ "<html>\n"
        			+ "<head>\n"
        			+ "    <title>Homepage</title>\n"
        			+ "</head>\n"
        			+ "<body>";
        	res += "<p>" + "Welcome " + user + "</p>";
        	//create channel
        	res += "<form method=\"GET\" action=\"/create\">\n"
        			+ "Channel Name: "
        			+ "<input type=\"text\" name=\"name\" />\n"
        			+ "XPath: " 
        			+ "<input type=\"text\" name=\"xpath\" />\n"
        			+"<input type=\"submit\" value=\"Create Channel\"/>\n"
        			+ "</form>";
        	//add channel info
        	PrimaryIndex<String, Channel> channels = database.channelIndex;
        	res += "<p> Current channels available: </p>\n";
        	if (channels.count()!=0) {
        		for (String cname: channels.keys()) {
            		res += "<li>" + cname + "<a href=/show?channel=" + cname+">" + "link" +"</a></li>";
            	}
        	}
        	res += "\n";
        	res += "<form method=\"POST\" action=\"/logout\">\n"
        			+ "<input type=\"submit\" value=\"log out\"/>\n"
        			+ "</form>";
        	res += "</body></html>";
//        	response.body(res);
        	return res;
        });
        
        get("/login-form.html", (req, res)->{
        	res.redirect("/login");
        	return null;
        });
        
        get("/login", (request, response)->{
        	if (request.session(false) == null) {
        		String	res = "<!DOCTYPE html>\n"
        				+ "<html>\n"
        				+ "<head>\n"
        				+ "    <title>Log in</title>\n"
        				+ "</head>\n"
        				+ "<body>\n"
        				+ "<h1>Log into Milestone 2</h1>\n"
        				+ "<p>If you don't have an account, please <a href=\"/register\">register<a> first.</p>\n"
        				+ "\n"
        				+ "<form method=\"POST\" action=\"/login\">\n"
        				+ "Name: <input type=\"text\" name=\"username\"/><br/>\n"
        				+ "Password: <input type=\"password\" name=\"password\"/><br/>\n"
        				+ "<input type=\"submit\" value=\"Log in\"/>\n"
        				+ "</form>\n"
        				+ "\n"
        				+ "</body>\n"
        				+ "</html>";
        		return res;
            } else {
            	request.attribute("user", request.session().attribute("user"));
            	response.redirect("/index.html");
            }
        	return "";
        });	
        
        get("/register.html", (req,  res)->{
        	res.redirect("/register");
        	return null;
        });
        
        get("/register", (request, response)->{
        	
        	if (request.session(false) == null) {
                String res = "<!DOCTYPE html>\n"
                		+ "<html>\n"
                		+ "<head>\n"
                		+ "    <title>Register account</title>\n"
                		+ "</head>\n"
                		+ "<body>\n"
                		+ "<h1>Create Account for Milestone 2</h1>\n"
                		+ "<p>Please register a user name and password</p>\n"
                		+ "\n"
                		+ "<form method=\"POST\" action=\"/register\">\n"
                		+ "Name: <input type=\"text\" name=\"username\"/><br/>\n"
                		+ "Password: <input type=\"password\" name=\"password\"/><br/>\n"
                		+ "<input type=\"submit\" value=\"Create account\"/>\n"
                		+ "</form>\n"
                		+ "\n"
                		+ "</body>\n"
                		+ "</html>";
                return res;
            } else {
            	request.attribute("user", request.session().attribute("user"));
            	response.redirect("/index.html");
            }
        	return "";
        });
        get("/lookup", (request, response)->{
        	String url = request.queryParams("url");
        	byte[] content = database.getDocument(url);
        	String contentType = database.getDocumentType(url);
        	if (content==null) {
        		halt(404, "Not Found");
        	}
//        	response.body(content);
        	response.type(contentType);
        	return content;
        });
        
        get("/create", (request, response)->{
        	Session s = request.session(false);
        	if (s==null) {
        		return null;
        	}
        	String channelName = request.queryParams("name");
        	String XPath = request.queryParams("xpath");
        	if (!XPathEngineImplementation.isValid(XPath)) {
        		halt(403, "invalid XPath expression");
        	};
        	response.redirect("/create/" + channelName + "?"+"xpath=" + XPath);
        	return null;
        });
        
        //TODO: add channel creation post request, and add db functionality
        get("/create/:name", (request, response)->{
        	Session s = request.session(false);
        	if (s==null) {
        		return null;
        	}
        	String channelName = request.params("name");
        	String XPath = request.queryParams("xpath");
        	logger.info("channel name, xpath are {} {}", channelName, XPath);
        	if (XPath==null) {
        		halt(403, "XPath is null");
        	}
        	String user = (String) s.attribute("user");
        	logger.info("current user is {}", user);
        	if (database.containsChannel(channelName)) {
        		halt(409, "Channel already exists");
        	}
        	Channel channel = new Channel(channelName, user, XPath);
        	database.addChannel(channel);
        	//TODO: add db.sync()
        	database.sync();
        	String res = "<!DOCTYPE html>\n"
            		+ "<html>\n"
            		+ "<body>\n"
            		+ "<p>Channel successfully added</p>\n"
            		+ "<p>Get back to <a href=\"/index.html\">Homepage<a></p>\n"
            		+ "</body>\n"
            		+ "</html>";
        	return res;
        	
        });
        get("/show", (request, response)->{
        	String channelName = request.queryParams("channel");
        	if (channelName==null) {
        		halt(403, "Channel name is null");
        	}
        	Session s = request.session(false);
        	Channel channel = database.getChannel(channelName);
        	if (channel==null) {
        		halt(404, "Channel not exists");
        	}
        	String creator = channel.creator;
        	List<String> matchingUrls = channel.matchingUrls;
        	String res = "<!DOCTYPE html>\n"
            		+ "<html>\n"
            		+ "<body>\n"
            		+ "<div class=\"channelheader\">Channel name: " + channelName + ", created by: " + creator + "</div>\n";
        	if (matchingUrls.size()==0) {
        		res += "<div>No matching documents for this channel</div>";
        	} else {
        		for (String url: matchingUrls) {
        			logger.info("retrieving url {}", url);
        			DocFile doc = database.getDocumentObject(url);
        			long dateVal = doc.lastCrawled;
        			Date date = new Date(dateVal);
        			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        			String dateStr = format.format(date);
        			res += "Crawled on: " + dateStr + " " + "Location: " + url + "\n";
        			//TODO: display document info in a brand new website, not just show in the same page
        			res += "<div class=\"document\">\n" + new String(doc.content) + "\n</div>";
        		}
        	}
        	res +=  "</body>\n"
            		+ "</html>";
        	return res;
        });
        
        
        awaitInitialization();
    }
}
