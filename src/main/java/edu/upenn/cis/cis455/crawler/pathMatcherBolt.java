package edu.upenn.cis.cis455.crawler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.upenn.cis.cis455.storage.Channel;
import edu.upenn.cis.cis455.xpathengine.OccurrenceEvent;
import edu.upenn.cis.cis455.xpathengine.XPathEngineFactory;
import edu.upenn.cis.cis455.xpathengine.XPathEngineImplementation;
import edu.upenn.cis.cis455.xpathengine.XPathInfo;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;

public class pathMatcherBolt implements IRichBolt {
	static final Logger logger = LogManager.getLogger(pathMatcherBolt.class);
	String executorId = UUID.randomUUID().toString();
	static Crawler crawler;
	OutputCollector collector;
	
	public pathMatcherBolt() {
	}
	
	public static void setCrawler(Crawler c) {
		crawler = c;
	}
	
	
	
	@Override
	public String getExecutorId() {
		// TODO Auto-generated method stub
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
	}

	@Override
	public void cleanup() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void execute(Tuple input) {
		// TODO Auto-generated method stub
		OccurrenceEvent event = (OccurrenceEvent) input.getObjectByField("event");
		String url = input.getStringByField("url");
		XPathEngineImplementation engine = crawler.engine;
		Map<String,Channel> XPathsMap = crawler.currentdb.getXPaths().map();
		List<String> XPaths =  new ArrayList<String>();
		for(XPathInfo X: crawler.engine.xp){
			XPaths.add(X.rawPath);
		}
		logger.info(XPaths);
		Map<String, String> pathToChannelName = new HashMap<String, String>();
		for (Channel c: XPathsMap.values()) {
			logger.info("xpath in db is {}", c.XPath);
			pathToChannelName.put(c.XPath, c.name);
		}
		boolean[] matching = engine.evaluateEvent(event);
		
		for (int i=0; i<matching.length;i++) {
			if (matching[i]) {
				String name = pathToChannelName.get(crawler.engine.xpMap.get(XPaths.get(i)));
				logger.info("matching {} to {}, with name {}", url, crawler.engine.xpMap.get(XPaths.get(i)), name);
				crawler.currentdb.addDocToChannel(name, url);
			}
		}
		crawler.currentdb.sync();
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		// TODO Auto-generated method stub
		this.collector = collector;
		
	}

	@Override
	public void setRouter(IStreamRouter router) {
		// TODO Auto-generated method stub
		collector.setRouter(router);
	}

	@Override
	public Fields getSchema() {
		// TODO Auto-generated method stub
		return null;
	}
	
}