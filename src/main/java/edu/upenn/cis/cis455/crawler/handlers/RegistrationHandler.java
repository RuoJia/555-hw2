package edu.upenn.cis.cis455.crawler.handlers;

import spark.Request;
import spark.Route;
import spark.Response;
import spark.HaltException;
import spark.Session;
//import edu.upenn.cis.cis455.m2.handling.*;
//import edu.upenn.cis.cis455.m2.interfaces.*;
//import edu.upenn.cis.cis455.m2.implementations.*;
import edu.upenn.cis.cis455.storage.Storage;
import edu.upenn.cis.cis455.storage.StorageInterface;

public class RegistrationHandler implements Route {
    Storage db;

    public RegistrationHandler(StorageInterface db) {
        this.db = (Storage) db;
    }

    @Override
    public String handle(Request req, Response resp) throws HaltException {
        String user = req.queryParams("username");
        String pass = req.queryParams("password");

        System.err.println("Register request for " + user + " and " + pass);
        String res="";
        if (db.containsUser(user)) {
        	res = "<!DOCTYPE html>\n"
        			+ "<html>\n"
        			+ "<head>\n"
        			+ "    <title>Register Error</title>\n"
        			+ "</head>\n"
        			+ "<body>";
        	res += "<h2>"
        			+ "Username already exits, try a different username" + "</h2>"
        			+ "<a href=/register>Back to Registration page</a></body></html>";
        	resp.body(res);
        } else {
        	db.addUser(user, pass);
        	db.sync();
        	res = "<!DOCTYPE html>\n"
        			+ "<html>\n"
        			+ "<head>\n"
        			+ "    <title>Registration Successful</title>\n"
        			+ "</head>\n"
        			+ "<body>";
        	res += "<h2>"
        			+ "<a href=/login>registration successfull, please log in</a></body></html>";
        	resp.body(res);
        }

        return res;
    }
}
