package edu.upenn.cis.cis455.crawler;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.net.ssl.HttpsURLConnection;

import edu.upenn.cis.cis455.crawler.utils.URLInfo;
import edu.upenn.cis.cis455.storage.Storage;
import edu.upenn.cis.stormlite.OutputFieldsDeclarer;
import edu.upenn.cis.stormlite.TopologyContext;
import edu.upenn.cis.stormlite.bolt.IRichBolt;
import edu.upenn.cis.stormlite.bolt.OutputCollector;
import edu.upenn.cis.stormlite.routers.IStreamRouter;
import edu.upenn.cis.stormlite.tuple.Fields;
import edu.upenn.cis.stormlite.tuple.Tuple;
import edu.upenn.cis.stormlite.tuple.Values;

public class fetcherBolt implements IRichBolt {
	static final Logger logger = LogManager.getLogger(fetcherBolt.class);
	String executorId = UUID.randomUUID().toString();
	OutputCollector collector;
	Storage db;
	static Crawler crawler;
	
	public fetcherBolt() {
	}
	
	public static void setCrawler(Crawler c) {
		crawler = c;
	}
	
	
	@Override
	public String getExecutorId() {
		// TODO Auto-generated method stub
		return executorId;
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		// TODO Auto-generated method stub
		declarer.declare(getSchema());
	}

	@Override
	public void cleanup() {
		// TODO add logger.debug/info
		
	}
	
    public Object[] sendHead(String currentUrl) throws IOException {
    	logger.info("cralwer sending head request to {}", currentUrl);
    	if (currentUrl.startsWith("https")) {
    		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
    		URL httpsUrl = new URL(currentUrl);
    		HttpsURLConnection cn = (HttpsURLConnection) httpsUrl.openConnection();
    		cn.setRequestMethod("HEAD");
    		cn.addRequestProperty("Host", httpsUrl.getHost());
    		cn.addRequestProperty("User-agent", "cis455crawler");
    		cn.connect();
    		int contentLength = -1;
			long lastModified  = -1;
			String contentType = "text/html";
    		int statusCode =  cn.getResponseCode();
    		if (statusCode >=200 && statusCode <=400) {
    			contentLength = cn.getContentLength();
    			lastModified = cn.getLastModified();
    			contentType = cn.getContentType();
    			return new Object[] {contentLength, contentType, lastModified, true};	
    		}
    	} else if (currentUrl.startsWith("http")) {
    		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
    		URL httpsUrl = new URL(currentUrl);
    		HttpURLConnection cn = (HttpURLConnection) httpsUrl.openConnection();
    		cn.setRequestMethod("HEAD");
    		cn.addRequestProperty("Host", httpsUrl.getHost());
    		cn.addRequestProperty("User-agent", "cis455crawler");
    		cn.connect();
    		int contentLength = -1;
			long lastModified  = -1;
			String contentType = "text/html";
    		int statusCode =  cn.getResponseCode();
    		if (statusCode >=200 && statusCode <=400) {
    			contentLength = cn.getContentLength();
    			lastModified = cn.getLastModified();
    			contentType = cn.getContentType();
    			return new Object[] {contentLength, contentType, lastModified, true};	
    		}
    	}
    	
    	return new Object[]{null, null, null, false};
    }
    
	public byte[] getContent(String url) throws IOException {
		// TODO Auto-generated method stub
		InputStream in=null;
		//send get request, return inputstream
		try {
			in = sendGet(url);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (in!=null) {
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			in.transferTo(os);
			return os.toByteArray();
		}		
		return null;
	}
	
    public InputStream sendGet(String currentUrl) throws IOException {
    	logger.info("crawler sending get request to {}", currentUrl);
    	if (currentUrl.startsWith("https")) {
    		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
    		URL httpsUrl = new URL(currentUrl);
    		HttpsURLConnection cn = (HttpsURLConnection) httpsUrl.openConnection();
    		cn.setRequestMethod("GET");
    		cn.addRequestProperty("Host", httpsUrl.getHost());
    		cn.addRequestProperty("User-agent", "cis455crawler");
    		cn.connect();
    		return cn.getInputStream();
    	} else if (currentUrl.startsWith("http")) {
    		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
    		URL httpsUrl = new URL(currentUrl);
    		HttpURLConnection cn = (HttpURLConnection) httpsUrl.openConnection();
    		cn.setRequestMethod("GET");
    		cn.addRequestProperty("Host", httpsUrl.getHost());
    		cn.addRequestProperty("User-agent", "cis455crawler");
    		cn.connect();
    		return cn.getInputStream();
    	}
    	return null;
    }
    
    
    public boolean correctType(String type) {
    	if (type.startsWith("text/html") || type.startsWith("text/xml") 
    			|| type.startsWith("application/xml") || type.endsWith("+xml")) {
    		return true;
    	}
    	return false;
    }
    
    public boolean validContentSize(int contentSize) {
    	if (contentSize >= crawler.maxSize) {
    		return false;
    	} 
    	return true;
    }
    
    public List<String> findUrls(String currentUrl){
    	List<String> nextUrls = new ArrayList<String>();
    	Document document;
    	try {
    		document = Jsoup.connect(currentUrl).get();
    		Elements es = document.select("a[href]");
    		for (Element e: es) {
    			String absHref = e.attr("abs:href");
    			nextUrls.add(absHref);
    		}
    	} catch (IOException e){
    		return nextUrls;
    	}
    	return nextUrls;
    }
    
	
	@Override
	public void execute(Tuple input) {
		// TODO Auto-generated method stub
		String currentUrl=input.getStringByField("url");

//		logger.info("current url is {}", currentUrl);
		URLInfo urlInfo = new URLInfo(currentUrl);
		
		
		//check if we follow robot.txt
		int contentLength = -1;
		long lastModified  = -1;
		String contentType = "text/html";
		String hostName = urlInfo.getHostName();
		Robot robot= null;
		synchronized(crawler.robotMap) {
			if (!crawler.robotMap.containsKey(hostName)) {
				//host name has been crawled, we need to add it to robot to check for delay and disallow
				robot = new Robot(hostName);
				crawler.robotMap.put(hostName, robot);
			} else {
				robot = crawler.robotMap.get(hostName);
			}
		}
		//check delay and disallow
//		logger.info("checking disallow");
		if(robot.isDisallowed(currentUrl)) {
			System.out.println(currentUrl + " disallows robot");
			return;
		}
//		logger.info("checking delay");
		if (robot.isDelay()) {
			try {
				crawler.addTask(currentUrl);
			} catch (Exception e) {
				System.out.println(e);
			}
			return;
		}
		//send head request
		boolean valid=false;
		Object[] returnVals=new Object[] {null, null, null};
		try {
			returnVals = sendHead(currentUrl);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		valid = (boolean) returnVals[3];
		if (valid) {
			contentLength = (int) returnVals[0];
			contentType = (String)returnVals[1];
			lastModified = (long) returnVals[2];
		}
		
		if (!valid) {
			System.out.println(currentUrl+" head request error");
			return;
		}
		if (!correctType(contentType)) {
			System.out.println(currentUrl+" content type error");
			return;
		}
		if (!validContentSize(contentLength)) {
			System.out.println(currentUrl+" content size error");
			return;
		}
		// process valid url and content
//		byte[] urlContent = getContent(currentUrl);
		byte[] urlContent=null;
		try {
			urlContent = getContent(currentUrl);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if (urlContent!=null) {
			String hashContent= "";
			try {
				hashContent = crawler.currentdb.getMd5(urlContent);
			} catch (NoSuchAlgorithmException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if ((crawler.currentdb.containsContent(currentUrl) && !crawler.currentdb.modified(currentUrl, lastModified))
					) {
				//checks if the crawler crawls the file in the previous run
				crawler.crawledUrls.add(currentUrl);
				if (!crawler.currentdb.contentSeen.contains(hashContent)) {
					collector.emit(new Values<Object>(currentUrl, urlContent, contentType));
				}
				logger.info("not modified {}", currentUrl);
			} else if (crawler.currentdb.contentSeen.contains(hashContent)) {
				//checks if the crawler saw the file in the same run
				//if either is true, then the file should not be downloaded
				logger.info("content has been crawled earlier {}", currentUrl);
				return;
			} else {
				logger.info("downloading {}", currentUrl);
				crawler.crawledUrls.add(currentUrl);
				robot.markVisitTime();	
				long lastCrawled = Calendar.getInstance().getTimeInMillis();
				crawler.currentdb.addDocument(currentUrl, urlContent, contentType, lastModified, lastCrawled);
				crawler.currentdb.sync();
				crawler.incCount();
				logger.info("added to db, now downloaded {} docs", crawler.currentPageNum);
				collector.emit(new Values<Object>(currentUrl, urlContent, contentType));
			}
			// extract other urls in currenturl
			if (contentType.contains("html") || contentType.contains("htm")) {
				List<String> urls = findUrls(currentUrl);
				logger.info("found {} urls", urls.size());
				for (String url: urls) {
					logger.info("{} has link to url: {}", currentUrl, url);
					if (!url.contains(".xml") && !url.contains(".html") && !url.contains("htm")) {
						//it's a directory html
						if (!url.endsWith("/")) {
							url += "/";
						}
					}
					if (!crawler.seenUrl.contains(url)) {
						try {
							crawler.addTask(url);
							synchronized(crawler.seenUrl) {
								crawler.seenUrl.add(url);
							}
						} catch (Exception e) {
							logger.info(e);
						}
					}else {	
						logger.info("url {} is seen, not added", url);
					}
				}
			}
			logger.info("urls added");		
		}
	}

	@Override
	public void prepare(Map<String, String> stormConf, TopologyContext context, OutputCollector collector) {
		// TODO Auto-generated method stub
		this.collector = collector;
	}

	@Override
	public void setRouter(IStreamRouter router) {
		// TODO Auto-generated method stub
		this.collector.setRouter(router);
	}

	@Override
	public Fields getSchema() {
		// TODO Auto-generated method stub
		return new Fields("url", "urlContent", "type");
	}
	
}