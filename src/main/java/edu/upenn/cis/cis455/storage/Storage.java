package edu.upenn.cis.cis455.storage;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.sleepycat.je.Environment;
import com.sleepycat.je.EnvironmentConfig;
import com.sleepycat.persist.EntityStore;
import com.sleepycat.persist.PrimaryIndex;
import com.sleepycat.persist.StoreConfig;


public class Storage implements StorageInterface {
	private Environment env;
	private EntityStore store;
	public PrimaryIndex<String, User> userIndex;
	public PrimaryIndex<String, DocFile> contentIndex;
	public PrimaryIndex<String, Channel> channelIndex;
	public Set<String>contentSeen = new HashSet<String>();
	
	static final Logger logger = LogManager.getLogger(Storage.class);
	
	public String getMd5(byte[] input) throws NoSuchAlgorithmException {

        MessageDigest mess = MessageDigest.getInstance("MD5"); 

        byte[] md = mess.digest(input); 

        BigInteger number = new BigInteger(1, md); 

        String hashed = number.toString(16); 
        while (hashed.length() < 32) { 
        	hashed = "0" + hashed; 
        } 
        return hashed; 
	}
	
	public String getSHA(String input) throws NoSuchAlgorithmException {

        MessageDigest mess = MessageDigest.getInstance("SHA-256"); 

        byte[] md = mess.digest(input.getBytes()); 

        BigInteger number = new BigInteger(1, md); 

        String hashed = number.toString(16); 
        while (hashed.length() < 32) { 
        	hashed = "0" + hashed; 
        } 
        return hashed; 
	}
	
	public boolean modified(String url, long lastModified) {
		DocFile doc = this.contentIndex.get(url);
		if (doc.lastModified == lastModified) {
			return false;
		}
		return true;
	}
	
	
	public Storage(String directory) {
		EnvironmentConfig envConf = new EnvironmentConfig();
		
		if (!Files.exists(Paths.get(directory))) {
            try {
                Files.createDirectory(Paths.get(directory));
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
		
		
		envConf.setAllowCreate(true);
		File dir = new File(directory);
		env = new Environment(dir, envConf);
		
		StoreConfig sc = new StoreConfig();
		sc.setAllowCreate(true);
		
		store = new EntityStore(env,"EntityStore", sc);
		
		userIndex = store.getPrimaryIndex(String.class, User.class);
		contentIndex = store.getPrimaryIndex(String.class, DocFile.class);
		channelIndex = store.getPrimaryIndex(String.class, Channel.class);
		System.out.println("database has been set up");
	}

	@Override
	public int getCorpusSize() {
		// TODO Auto-generated method stub
		return (int) contentIndex.count();
	}

	@Override
	public synchronized int addDocument(String url, byte[] documentContents, String contentType, long lastModified, long lastCrawled) {
		// TODO Auto-generated method stub
		Random rand = new Random();
		int id = rand.nextInt();
		DocFile doc = new DocFile(url, documentContents, id, contentType, lastModified, lastCrawled);
		contentIndex.put(doc);
		
		//add to contentSeen
		String hashContent  = null;
		try {
			hashContent = getMd5(documentContents);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (hashContent!=null) this.contentSeen.add(hashContent);
		return id;
	}

	@Override
	public byte[] getDocument(String url) {
		// TODO Auto-generated method stub
//		logger.info(contentIndex.get(url).content);
		if (contentIndex.get(url)!=null) {
			return contentIndex.get(url).content;
		} else {
			return null;
		}
	}
	
	public DocFile getDocumentObject(String url) {
		if (contentIndex.get(url)!=null) {
			logger.info("url {} is found", url);
			return contentIndex.get(url);
		} else {
			logger.info("url {} is not found", url);
			return null;
		}
	}
	
	public String getDocumentType(String url) {
		if (contentIndex.get(url)!=null) {
			return contentIndex.get(url).contentType;
		} else {
			return null;
		}
	}

	@Override
	public int addUser(String username, String password) {
		// TODO Auto-generated method stub
		Random rand = new Random();
		int id = rand.nextInt();
		String hashpw=password;
		try {
			hashpw = getSHA(password);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		User user = new User(username, hashpw, id);
		userIndex.put(user);
		return id;
	}

	@Override
	public boolean getSessionForUser(String username, String password) {
		// TODO Auto-generated method stub
		String hashpw="";
		try {
			hashpw = getSHA(password);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (this.containsUser(username) && this.userIndex.get(username).password.equals(hashpw)) {
			return true;
		}
		return false;
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub
		if (env!=null) {
			env.close();
		}
		if (store!=null) {
			store.close();
		}
	} 
	
	public boolean containsContent(String url) {
		//TODO: Redundant documents and cyclic crawls: Your crawler should compute an MD5 hash of every
//		document that is fetched, and store this in a “content seen” table in BerkeleyDB. If you crawl a
//		document with a matching hash during the same crawler run, you should not index it or traverse its
//		outgoing links.
		return contentIndex.contains(url);
	}
	
	public boolean containsUser(String username) {
		logger.info("contains user {} ? {}", username, userIndex.contains(username));
		return userIndex.contains(username);
	}
	
	public PrimaryIndex<String, Channel> getXPaths(){
		return channelIndex;
	}
	
	public List<String> getXPathList(){
		Map<String,Channel> XPathsMap = channelIndex.map();
		List<String> XPaths =  new ArrayList<String>();
		for (Channel c: XPathsMap.values()) {
			XPaths.add(c.XPath);
		}
		return XPaths;
	}
	
	public synchronized void addDocToChannel(String name, String url) {
		Channel c = channelIndex.get(name);
		if (!c.matchingUrls.contains(url)) {
			c.addMatchingUrls(url);
		}
		channelIndex.put(c);
	}
	
	public boolean containsChannel(String channelName) {
		return channelIndex.contains(channelName);
	}
	
	public void addChannel(Channel c) {
		channelIndex.put(c);
	}
	
	public Channel getChannel(String cname) {
		return channelIndex.get(cname);
	}


	public synchronized void sync() {
		// TODO Auto-generated method stub
		
		if (env!=null) {
			env.sync();
		}
		if (store!=null) {
			store.sync();
		} 
	}
	
}