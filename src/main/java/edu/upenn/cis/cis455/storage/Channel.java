package edu.upenn.cis.cis455.storage;
import java.util.ArrayList;
import java.util.List;

import com.sleepycat.persist.model.*;

@Entity
public class Channel {
	@PrimaryKey
	public String name;
	public String creator;
	public String XPath;
	public List<String> matchingUrls = new ArrayList<String>();
	public Channel() {}
	
	public Channel(String name, String creator, String XPath) {
		this.creator = creator;
		this.name = name;
		this.XPath = XPath;
	}
	
	public void addMatchingUrls(String url) {
		if (!matchingUrls.contains(url)) {
			matchingUrls.add(url);
		}
	}
}