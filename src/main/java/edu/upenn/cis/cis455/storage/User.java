package edu.upenn.cis.cis455.storage;
import com.sleepycat.persist.model.*;
@Entity 
public class User{
	@PrimaryKey
	public String username;
	public String password;
	private int id;
	
	public User() {};
	public User(String username, String password, int id) {
		this.username = username;
		this.password =  password;
		this.id = id;
	}
}