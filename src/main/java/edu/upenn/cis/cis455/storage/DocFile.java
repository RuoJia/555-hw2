package edu.upenn.cis.cis455.storage;

import com.sleepycat.persist.model.*;

@Entity 
public class DocFile{
	@PrimaryKey
	public String url;
	public byte[] content;
	private int id;
	public String contentType;
	public long lastModified;
	public long lastCrawled;
	
	public DocFile() {};
	public DocFile(String url, byte[] content, int id, String contentType, long lastModified, long lastCrawled) {
		this.url = url;
		this.content =  content;
		this.id = id;
		this.contentType = contentType;
		this.lastModified = lastModified;
		this.lastCrawled = lastCrawled;
	}
}