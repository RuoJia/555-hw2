package edu.upenn.cis.cis455.xpathengine;
import java.io.IOException;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

public class parser {
	public static void main(String args[]){
		Document doc=null;
		try {
			doc = Jsoup.connect("https://crawltest.cis.upenn.edu/misc/weather.xml").get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("doc name is "+ doc.nodeName());
		List<Element> childNodes = doc.children();
		for (Element e: childNodes) {
			System.out.println("main node "  + " "+e.nodeName() + " " + e.ownText() + " " + e.baseUri());
			for (Element e2: e.children()) {
				System.out.println("sub node "  + " "+e2.nodeName() + " " + e2.ownText() + " " + e2.baseUri());
				for (Element e3: e2.children()) {
					System.out.println("sub-sub node "  + " "+e3.nodeName() + " " + e3.ownText()+ " " + e3.baseUri());
				}
			}
		}
	}
}