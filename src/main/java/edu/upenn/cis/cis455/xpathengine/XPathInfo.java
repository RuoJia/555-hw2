package edu.upenn.cis.cis455.xpathengine;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XPathInfo {
	public Map<String, String> containsMap =new HashMap<String, String>();
	public Map<String, String> haveMap=new HashMap<String, String>();
	public String pathValue="";
	public String rawPath = "";
	public XPathInfo(String path) {
		rawPath = path;
		path = path.substring(1);
		String[] parts = path.split("/");
		for (String s: parts) {
			if (s.contains("[")) {
				String temp = s;
				String nodeName = s.substring(0, s.indexOf("["));
				pathValue += "<" + nodeName+ ">";
				System.out.println("xpath nodeName is "+ nodeName);
				while (temp.contains("[")) {
					int startIndex = temp.indexOf("[");
					int endIndex = temp.indexOf("]");
					String test = temp.substring(startIndex+1, endIndex);
					int sIndex = test.indexOf("\"");
					int eIndex = test.lastIndexOf("\"");
					String textContent = test.substring(sIndex+1, eIndex).toLowerCase();
					if (test.contains("contains")) {
						//contains(text(), "")
						System.out.println("xpath contains is "+ textContent);
						containsMap.put(nodeName, textContent);
					} else {
						//text() = ""
						System.out.println("xpath exact match is "+ textContent);
						haveMap.put(nodeName, textContent);
					}
					temp = temp.substring(endIndex+1);
				}
				
			} else {
				//only node
				pathValue += "<" + s + ">";
			}
		}
	}
}