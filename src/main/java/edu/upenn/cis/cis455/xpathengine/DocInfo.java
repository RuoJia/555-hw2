package edu.upenn.cis.cis455.xpathengine;

import java.util.HashMap;
import java.util.Map;

public class DocInfo{
	String baseUrl = "";
	public String currentPathInfo = "";
	public Map<String, String>pathText = new HashMap<String, String>();
	public DocInfo(String url) {
		baseUrl = url;
	}
}