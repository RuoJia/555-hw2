package edu.upenn.cis.cis455.xpathengine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class XPathEngineImplementation implements XPathEngine {
	public List<XPathInfo> xp = new ArrayList<XPathInfo>();
	public Map<String, String>xpMap = new HashMap<String, String>();
	public Map<String, DocInfo> currentState = new HashMap<String, DocInfo>();
	static final Logger logger = LogManager.getLogger(XPathEngineImplementation.class);
	public XPathEngineImplementation() {}
	
	//TODO: add isValid(i) method
	//TODO: handle /*/* in XPath
	
	@Override
	public void setXPaths(String[] expressions) {
		// TODO Auto-generated method stub
		for (String exp: expressions) {
			String or = new String(exp);
			logger.info("or is {}", or);
			if (isValid(exp)) {
				String lower = exp.toLowerCase();
				if (lower.contains("html")) {
					exp = exp.toLowerCase();
					xp.add(new XPathInfo(exp));
				} else {
					xp.add(new XPathInfo(exp));
				}
				logger.info("valid xpath is {}", exp);
				xpMap.put(exp, or);
			}
		}
	}
	
	public boolean[] checkMatching(String url) {
		boolean[] returnVal=new boolean[xp.size()];
		for (int i=0; i<xp.size();i++) {
			returnVal[i] =true;
		}
		//1. check path matching
		//2. if path matches(meaning node pathinfo are correct, then check text info matches
		for (XPathInfo x: xp) {
			DocInfo currentDoc = currentState.get(url);
			if (x.pathValue.equals(currentDoc.currentPathInfo)) {
				//node path matches, then check text info
				//first, check contains
				for (Entry<String, String> entry: x.containsMap.entrySet()) {
					String nodeName = entry.getKey();
					if (currentDoc.pathText.get(nodeName)==null) {
						returnVal[xp.indexOf(x)] = false;
					}
					if (!currentDoc.pathText.get(nodeName).contains(entry.getValue())) {
						returnVal[xp.indexOf(x)] = false;
					}
				}
				//second, check exact match
				for (Entry<String, String> entry: x.haveMap.entrySet()) {
					String nodeName = entry.getKey();
					if (!entry.getValue().equals(currentDoc.pathText.get(nodeName))) {
						returnVal[xp.indexOf(x)] = false;
					}
				}
				//now, all have been checked, rest is true
			} else {
				returnVal[xp.indexOf(x)]=false;
			}
		}
		return returnVal;
	}

	@Override
	public boolean[] evaluateEvent(OccurrenceEvent event) {
		boolean[] returnVal=new boolean[xp.size()];
		// TODO Auto-generated method stub
		if (event.type == OccurrenceEvent.Type.Open) {
			//open event
			//1. add tag info
			if (!currentState.containsKey(event.url)) {
				currentState.put(event.url, new DocInfo(event.url));
			}
			DocInfo doc = currentState.get(event.url);
			doc.currentPathInfo += event.toString();
		} else if (event.type == OccurrenceEvent.Type.Text) {
			//text event
			//1. add text info
			//2. check path and text contains match

			DocInfo doc = currentState.get(event.url);
			String nodeName = doc.currentPathInfo.substring(doc.currentPathInfo.lastIndexOf("<")+1, doc.currentPathInfo.lastIndexOf(">"));
			doc.pathText.put(nodeName, event.toString());
			returnVal = checkMatching(event.url);
		} else {
			//close event
			//1.delete tag info
			//2.delete text contain info
			DocInfo doc = currentState.get(event.url);
			String nodeName = doc.currentPathInfo.substring(doc.currentPathInfo.lastIndexOf("<")+1, doc.currentPathInfo.lastIndexOf(">"));
			doc.pathText.remove(nodeName);
			doc.currentPathInfo = doc.currentPathInfo.substring(0, doc.currentPathInfo.lastIndexOf("<"));
			//TODO: add removal doc info in xpath engine
//			if (doc.currentPathInfo.length()==0) {
//				currentState.remove(event.url);
//			}
		}
		return returnVal;
	}
	
	public static boolean isValidTest(String test) {
		String[] testcases = {"^text\\(\\)=\".*\"$", "^contains\\(text\\(\\),\".*\"\\)$"};
		for (String c: testcases) {
			Pattern pt = Pattern.compile(c);
			Matcher mt = pt.matcher(test);
			if (mt.find()) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isValidStep(String step) {
		String stepRegex = "^([a-zA-Z_][a-zA-Z0-9_]*)(\\[.+\\])*$";
		Pattern pt = Pattern.compile(stepRegex);
		Matcher mt = pt.matcher(step);
		if (mt.find()) {
			String test = mt.group(2);
			//if test is null, then it is already valid
			if (test!=null) {
				while (test.contains("[")) {
					int endIndex = test.indexOf("]");
					if (!isValidTest(test.substring(1, endIndex))) {
						return false;
					} 
					test = test.substring(endIndex+1);
				}
			}
		} else {
			return false;
		}
		return true;
	}
	
	public static boolean isValid(String xpath) {
		xpath = xpath.trim().replaceAll(" ", "");
		logger.info("xpath trimmed is {}", xpath);
		if (xpath.length()==0 || !xpath.startsWith("/")) {
			return false;
		}
		xpath = xpath.substring(1);
		String[] parts = xpath.split("/");
		for (String p: parts) {
			logger.info("each step is {}", p);
			if (!isValidStep(p)){
				return false;
			}
		}
		return true;
	}
	
	
}